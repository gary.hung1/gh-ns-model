const { fileURLToPath } = require('node:url');
// const path = require("path");

exports.absPath = function(){
  // return path.resolve(__filename);
  const __filename = fileURLToPath(new URL(import.meta.url));
  return __filename;
}
